#!/bin/sh
terraform_url=$(curl https://releases.hashicorp.com/index.json | jq '{terraform}' | egrep "linux.*amd64" | sort --version-sort -r | head -1 | awk -F[\"] '{print $4}')
curl -o terraform.zip $terraform_url
unzip terraform.zip
sudo mv terraform /usr/local/bin
rm terraform.zip
