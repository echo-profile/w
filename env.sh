#!/bin/sh
export GOPATH=/home/fromz/go
export PATH=$PATH:/usr/lib/go-1.10/bin:$GOPATH/bin

export TMPDIR=/tmp
qq() {
    clear
    local gpath="${GOPATH:-$HOME/go}"
    "${gpath%%:*}/src/github.com/y0ssar1an/q/q.sh" "$@"
}
rmqq() {
    if [[ -f "$TMPDIR/q" ]]; then
        rm "$TMPDIR/q"
    fi
    qq
}
